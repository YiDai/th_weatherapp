import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonArray;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;

import java.io.IOException;
import java.net.URLConnection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;



public class Weather {
    private String zipcode;
    private final String ACCESS_TOKEN = "1655f919bbcd29ed";
    private JsonElement conditionsJson;
    private ArrayList<DayForecast> forecastList;
    Map<String, String> dName = new HashMap<>();
    Map<String, String> con = new HashMap<>();
    Map<String, String> I = new HashMap<>();
    Map<String, String> h = new HashMap<>();
    Map<String, String> l = new HashMap<>();

    public Weather(String zipcode) {
        zipcode = zipcode.replaceAll(" ", "_");
        this.zipcode = zipcode;
        conditionsJson = apiReturn("conditions");
        buildForecastList(apiReturn("forecast10day"));
    }

    public void buildForecastList(JsonElement fjson) {
        forecastList = new ArrayList<DayForecast>();
        String dayName;
        String conditions;
        String icon;
        String high;
        String low;
        int i = 1;

        JsonArray days = fjson.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray();
        for (JsonElement day : days) {
            //System.out.println(day);
            dayName = day.getAsJsonObject().get("date").getAsJsonObject().get("weekday").getAsString();
            conditions = day.getAsJsonObject().get("conditions").getAsString();
            icon = day.getAsJsonObject().get("icon").getAsString();
            high = day.getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString();
            low = day.getAsJsonObject().get("low").getAsJsonObject().get("fahrenheit").getAsString();
            dName.put("dayName" + i, dayName);
            con.put("con" + i, conditions);
            I.put("i" + i, icon);
            h.put("h" + i, high);
            l.put("l" + i, low);

            i++;

            //forecastList.add(new DayForecast(dayName, conditions, icon, high, low));
            //System.out.println(dayName);
            //System.out.println(conditions);
            //System.out.println(icon);
            //System.out.println(high);
            //System.out.println(low);
        }
        System.out.println(dName);
        System.out.println(con);

    }

    private JsonElement apiReturn(String apiEndpoint) {
        try {
            // Encode the user-supplied data to neutralize any special chars
            // String encodedURL = URLEncoder.encode(zipcode, "utf-8");

            // Construct API URL
            String apiURL = "https://api.wunderground.com/api/" +
                    ACCESS_TOKEN + "/" + apiEndpoint + "/q/" + zipcode + ".json";

            // Create URL object
            URL bitlyURL = new URL(apiURL);

            // Create InputStream Object
            InputStream is = bitlyURL.openStream();

            // Create InputStreamReader
            InputStreamReader isr = new InputStreamReader(is);

            // Parse input stream into a JsonElement
            JsonParser parser = new JsonParser();
            return parser.parse(isr);
        } catch (java.net.MalformedURLException mue) {
            System.out.println("Malformed URL");
        } catch (java.io.IOException ioe) {
            System.out.println("IO Error");
        }
        return null;
    }

    public String getRadar() {
        // animated link
        // https://stackoverflow.com/questions/10292792/getting-image-from-url-java
        // http://api.wunderground.com/api/1655f919bbcd29ed/animatedradar/q/95678/image.gif?newmaps=1&timelabel=1&timelabel.y=10&num=5&delay=50

        return ("http://api.wunderground.com/api/" + ACCESS_TOKEN + "/animatedradar/q/" + zipcode + "image.gif?newmaps=1&timelabel=1&timelabel.y=10&num=5");
    }


        /**
         * Returns the short Bitly URL
         */
    public String getCityState() {
        return conditionsJson.getAsJsonObject().get("current_observation").getAsJsonObject().get("display_location").getAsJsonObject().get("full").getAsString();
    }

    /**
     * Returns the hash portion of the short URL
     */
    public String getTemperature() {
        return conditionsJson.getAsJsonObject().get("current_observation").getAsJsonObject().get("temp_f").getAsString();
    }

    public String getWeather() {
        return conditionsJson.getAsJsonObject().get("current_observation").getAsJsonObject().get("weather").getAsString();
    }

    public String getIcon() {
        return conditionsJson.getAsJsonObject().get("current_observation").getAsJsonObject().get("icon").getAsString();
    }

   /* public void checkResults()
    {

        if(conditionsJson.getAsJsonObject().get("response").getAsJsonObject().has("results"))
        {
            ArrayList<String> list = new ArrayList<String>();
            JsonArray jr = new JsonArray();
            for(int i=0; i<jr.length();i++)
            {
                JSONObject jb = jr.getJSONObject(i);
                String value= jb.getString("UserID");
                list.add(value);
            }
        }
    }*/

    public static String getZip() {
        try{
            URL ipapi = new URL("https://ipapi.co/postal/");

            URLConnection c = ipapi.openConnection();
            c.setRequestProperty("User-Agent", "java-ipapi-client");
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(c.getInputStream())
            );
            String location = reader.readLine();
            reader.close();

            return location; }

        catch (java.io.IOException ioe){}
        return null;
    }



    /*public boolean checkErrors() {

        if (conditionsJson.getAsJsonObject().get("response").getAsJsonObject().has("error")) {

       System.out.println("TRUE");
        }

    else
        System.out.println("FALSE");
        return false;
    }*/


    public String getDay1() {
        return dName.get("dayName1");
    }

    public String getDay2() {
        return dName.get("dayName2");
    }

    public String getDay3() {
        return dName.get("dayName3");
    }

    public String getDay4() {
        return dName.get("dayName4");
    }

    public String getDay5() {
        return dName.get("dayName5");
    }

    public String getDay6() {
        return dName.get("dayName6");
    }

    public String getDay7() {
        return dName.get("dayName7");
    }

    public String getDay8() {
        return dName.get("dayName8");
    }

    public String getDay9() {
        return dName.get("dayName9");
    }

    public String getDay10() {
        return dName.get("dayName10");
    }

    public String getcon1() {
        return con.get("con1");
    }

    public String getcon2() {
        return con.get("con2");
    }

    public String getcon3() {
        return con.get("con3");
    }

    public String getcon4() {
        return con.get("con4");
    }

    public String getcon5() {
        return con.get("con5");
    }

    public String getcon6() {
        return con.get("con6");
    }

    public String getcon7() {
        return con.get("con7");
    }

    public String getcon8() {
        return con.get("con8");
    }

    public String getcon9() {
        return con.get("con9");
    }

    public String getcon10() {
        return con.get("con10");
    }

    public String getI1() {
        return I.get("i1");
    }

    public String getI2() {
        return I.get("i2");
    }

    public String getI3() {
        return I.get("i3");
    }

    public String getI4() {
        return I.get("i4");
    }

    public String getI5() {
        return I.get("i5");
    }

    public String getI6() {
        return I.get("i6");
    }

    public String getI7() {
        return I.get("i7");
    }

    public String getI8() {
        return I.get("i8");
    }

    public String getI9() {
        return I.get("i9");
    }

    public String getI10() {
        return I.get("i10");
    }

    public String geth1() {
        return h.get("h1");
    }

    public String geth2() {
        return h.get("h2");
    }

    public String geth3() {
        return h.get("h3");
    }

    public String geth4() {
        return h.get("h4");
    }

    public String geth5() {
        return h.get("h5");
    }

    public String geth6() {
        return h.get("h6");
    }

    public String geth7() {
        return h.get("h7");
    }

    public String geth8() {
        return h.get("h8");
    }

    public String geth9() {
        return h.get("h9");
    }

    public String geth10() {
        return h.get("h10");
    }

    public String getl1() {
        return l.get("l1");
    }

    public String getl2() {
        return l.get("l2");
    }

    public String getl3() {
        return l.get("l3");
    }

    public String getl4() {
        return l.get("l4");
    }

    public String getl5() {
        return l.get("l5");
    }

    public String getl6() {
        return l.get("l6");
    }

    public String getl7() {
        return l.get("l7");
    }

    public String getl8() {
        return l.get("l8");
    }

    public String getl9() {
        return l.get("l9");
    }

    public String getl10() {
        return l.get("l10");
    }

    public static void main(String[] args) {

        Weather b = new Weather(getZip());

        String shortened = b.getCityState();
        System.out.println(shortened);

        String hash = b.getTemperature();
        System.out.println(hash);

        String weather = b.getWeather();
        System.out.println(weather);

        String icon = b.getIcon();
        System.out.println(icon);

        String Day = b.getDay1();
        System.out.println(Day);



    }


}