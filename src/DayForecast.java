public class DayForecast
{
    String day;
    String conditions;
    String icon;
    int high,low;

    public DayForecast(String day, String conditions, String icon, int high, int low)
    {
        this.day = day;
        this.conditions = conditions;
        this.icon = "images/icons/" + icon + ".gif";
        this.high = high;
        this.low = low;

    }
}
