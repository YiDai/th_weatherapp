import acm.graphics.GCanvas;
import acm.graphics.GImage;
import acm.program.Program;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class GUI extends Program
{
    private JTextField inputField;
    private JTextField shortField;
    private JTextField hashResult;
    private JTextField weatherResult, Day1, Day2, Day3, Day4, Day5, Day6, Day7, Day8, Day9, Day10, Day1h, Day2h, Day3h, Day4h, Day5h, Day6h, Day7h, Day8h, Day9h, Day10h ;
    private JTextField Day1l, Day2l, Day3l, Day4l, Day5l, Day6l, Day7l, Day8l, Day9l, Day10l, Day1c, Day2c, Day3c, Day4c, Day5c, Day6c, Day7c, Day8c, Day9c, Day10c;
    private GImage Day1i, Day2i, Day3i, Day4i, Day5i, Day6i, Day7i, Day8i, Day9i, Day10i;
    private GImage icon;
    private GImage radar;

    private GImage bg;
    public GUI()
    {
        start();
    }

    public void init()
    {
        JLabel inputLabel = new JLabel("Zip Code");
        JLabel shortLabel = new JLabel("City and State");
        JLabel hashLabel = new JLabel("Current Temperature");
        JLabel weatherLabel = new JLabel("Weather");

        GCanvas canvas = new GCanvas();
        this.add(canvas);

        canvas.add(inputLabel, 20, 20);
        canvas.add(shortLabel, 20, 50);
        canvas.add(hashLabel, 20, 80);
        canvas.add(weatherLabel, 20, 110);

        inputField = new JTextField();
        inputField.setSize(300, 20);
        canvas.add(inputField, 160, 20);

        JButton goButton = new JButton("Go");
        canvas.add(goButton, 500, 20);

        JButton clearButton = new JButton("Clear");
        canvas.add(clearButton, 550, 20);

        shortField = new JTextField();
        shortField.setSize(300, 20);
        canvas.add(shortField, 160, 50);
        shortField.setEditable(false);

        hashResult = new JTextField();
        hashResult.setSize(300,20);
        canvas.add(hashResult, 160, 80);
        hashResult.setEditable(false);

        weatherResult = new JTextField();
        weatherResult.setSize(300,20);
        canvas.add(weatherResult, 160,110);
        weatherResult.setEditable(false);

        icon = new GImage("images/icons/blank.gif");
        icon.setSize(50,50);
        canvas.add(icon,500,80);

        bg = new GImage("images/backgrounds/white.png");
        bg.setSize(4000,4000);
        canvas.add(bg, 0, 0);

        int x = 90;
        int y = 180;
        Day1 = new JTextField();
        Day1.setSize(100,20);
        canvas.add(Day1,x,y);
        Day1.setEditable(false);
        Day2 = new JTextField();
        Day2.setSize(100,20);
        canvas.add(Day2,x,y+50);
        Day2.setEditable(false);
        Day3 = new JTextField();
        Day3.setSize(100,20);
        canvas.add(Day3,x,y+100);
        Day3.setEditable(false);
        Day4 = new JTextField();
        Day4.setSize(100,20);
        canvas.add(Day4,x,y+150);
        Day4.setEditable(false);
        Day5 = new JTextField();
        Day5.setSize(100,20);
        canvas.add(Day5,x,y+200);
        Day5.setEditable(false);
        Day6 = new JTextField();
        Day6.setSize(100,20);
        canvas.add(Day6,x,y+250);
        Day6.setEditable(false);
        Day7 = new JTextField();
        Day7.setSize(100,20);
        canvas.add(Day7,x,y+300);
        Day7.setEditable(false);
        Day8 = new JTextField();
        Day8.setSize(100,20);
        canvas.add(Day8,x,y+350);
        Day8.setEditable(false);
        Day9 = new JTextField();
        Day9.setSize(100,20);
        canvas.add(Day9,x,y+400);
        Day9.setEditable(false);
        Day10 = new JTextField();
        Day10.setSize(100,20);
        canvas.add(Day10,x,y+450);
        Day10.setEditable(false);
        Day1c = new JTextField();
        Day1c.setSize(100,20);
        canvas.add(Day1c,x+120,y);
        Day1c.setEditable(false);
        Day2c = new JTextField();
        Day2c.setSize(100,20);
        canvas.add(Day2c,x+120,y+50);
        Day2c.setEditable(false);
        Day3c = new JTextField();
        Day3c.setSize(100,20);
        canvas.add(Day3c,x+120,y+100);
        Day3c.setEditable(false);
        Day4c = new JTextField();
        Day4c.setSize(100,20);
        canvas.add(Day4c,x+120,y+150);
        Day4c.setEditable(false);
        Day5c = new JTextField();
        Day5c.setSize(100,20);
        canvas.add(Day5c,x+120,y+200);
        Day5c.setEditable(false);
        Day6c = new JTextField();
        Day6c.setSize(100,20);
        canvas.add(Day6c,x+120,y+250);
        Day6c.setEditable(false);
        Day7c = new JTextField();
        Day7c.setSize(100,20);
        canvas.add(Day7c,x+120,y+300);
        Day7c.setEditable(false);
        Day8c = new JTextField();
        Day8c.setSize(100,20);
        canvas.add(Day8c,x+120,y+350);
        Day8c.setEditable(false);
        Day9c = new JTextField();
        Day9c.setSize(100,20);
        canvas.add(Day9c,x+120,y+400);
        Day9c.setEditable(false);
        Day10c = new JTextField();
        Day10c.setSize(100,20);
        canvas.add(Day10c,x+120,y+450);
        Day10c.setEditable(false);
        Day1h = new JTextField();
        Day1h.setSize(100,20);
        canvas.add(Day1h,x+240,y);
        Day1h.setEditable(false);
        Day2h = new JTextField();
        Day2h.setSize(100,20);
        canvas.add(Day2h,x+240,y+50);
        Day2h.setEditable(false);
        Day3h = new JTextField();
        Day3h.setSize(100,20);
        canvas.add(Day3h,x+240,y+100);
        Day3h.setEditable(false);
        Day4h = new JTextField();
        Day4h.setSize(100,20);
        canvas.add(Day4h,x+240,y+150);
        Day4h.setEditable(false);
        Day5h = new JTextField();
        Day5h.setSize(100,20);
        canvas.add(Day5h,x+240,y+200);
        Day5h.setEditable(false);
        Day6h = new JTextField();
        Day6h.setSize(100,20);
        canvas.add(Day6h,x+240,y+250);
        Day6h.setEditable(false);
        Day7h = new JTextField();
        Day7h.setSize(100,20);
        canvas.add(Day7h,x+240,y+300);
        Day7h.setEditable(false);
        Day8h = new JTextField();
        Day8h.setSize(100,20);
        canvas.add(Day8h,x+240,y+350);
        Day8h.setEditable(false);
        Day9h = new JTextField();
        Day9h.setSize(100,20);
        canvas.add(Day9h,x+240,y+400);
        Day9h.setEditable(false);
        Day10h = new JTextField();
        Day10h.setSize(100,20);
        canvas.add(Day10h,x+240,y+450);
        Day10h.setEditable(false);
        Day1l = new JTextField();
        Day1l.setSize(100,20);
        canvas.add(Day1l,x+360,y);
        Day1l.setEditable(false);
        Day2l = new JTextField();
        Day2l.setSize(100,20);
        canvas.add(Day2l,x+360,y+50);
        Day2l.setEditable(false);
        Day3l = new JTextField();
        Day3l.setSize(100,20);
        canvas.add(Day3l,x+360,y+100);
        Day3l.setEditable(false);
        Day4l = new JTextField();
        Day4l.setSize(100,20);
        canvas.add(Day4l,x+360,y+150);
        Day4l.setEditable(false);
        Day5l = new JTextField();
        Day5l.setSize(100,20);
        canvas.add(Day5l,x+360,y+200);
        Day5l.setEditable(false);
        Day6l = new JTextField();
        Day6l.setSize(100,20);
        canvas.add(Day6l,x+360,y+250);
        Day6l.setEditable(false);
        Day7l = new JTextField();
        Day7l.setSize(100,20);
        canvas.add(Day7l,x+360,y+300);
        Day7l.setEditable(false);
        Day8l = new JTextField();
        Day8l.setSize(100,20);
        canvas.add(Day8l,x+360,y+350);
        Day8l.setEditable(false);
        Day9l = new JTextField();
        Day9l.setSize(100,20);
        canvas.add(Day9l,x+360,y+400);
        Day9l.setEditable(false);
        Day10l = new JTextField();
        Day10l.setSize(100,20);
        canvas.add(Day10l,x+360,y+450);
        Day10l.setEditable(false);

        Day1i = new GImage("images/icons/blank.gif");
        Day1i.setSize(30,30);
        canvas.add(Day1i,x+480,y);
        Day2i = new GImage("images/icons/blank.gif");
        Day2i.setSize(30,30);
        canvas.add(Day2i,x+480,y+50);
        Day3i = new GImage("images/icons/blank.gif");
        Day3i.setSize(30,30);
        canvas.add(Day3i,x+480,y+100);
        Day4i = new GImage("images/icons/blank.gif");
        Day4i.setSize(30,30);
        canvas.add(Day4i,x+480,y+150);
        Day5i = new GImage("images/icons/blank.gif");
        Day5i.setSize(30,30);
        canvas.add(Day5i,x+480,y+200);
        Day6i = new GImage("images/icons/blank.gif");
        Day6i.setSize(30,30);
        canvas.add(Day6i,x+480,y+250);
        Day7i = new GImage("images/icons/blank.gif");
        Day7i.setSize(30,30);
        canvas.add(Day7i,x+480,y+300);
        Day8i = new GImage("images/icons/blank.gif");
        Day8i.setSize(30,30);
        canvas.add(Day8i,x+480,y+350);
        Day9i = new GImage("images/icons/blank.gif");
        Day9i.setSize(30,30);
        canvas.add(Day9i,x+480,y+400);
        Day10i = new GImage("images/icons/blank.gif");
        Day10i.setSize(30,30);
        canvas.add(Day10i,x+480,y+450);

        radar = new GImage("images/icons/blankradar.gif");
        icon.setSize(100, 100);
        canvas.add(radar, 650, 80);

        icon = new GImage("images/icons/blank.gif");
        icon.setSize(50,50);
        canvas.add(icon,500,80);

        addActionListeners();
    }

    public static void main(String[] args)

    {

        GUI g= new GUI();

    }



    public void actionPerformed(ActionEvent ae)

    {

        String whichButton = ae.getActionCommand();



        switch (whichButton)

        {

            case "Go":

                Weather b = new Weather(inputField.getText());

                shortField.setText(b.getCityState());
                hashResult.setText(b.getTemperature());
                weatherResult.setText(b.getWeather());
                icon.setImage("images/icons/" + b.getIcon()+".gif");
                radar.setImage(b.getRadar());

                bg.setImage("images/backgrounds/" + b.getIcon()+ ".png");

                bg.setSize(4000, 4000);

                Day1.setText(b.getDay1());
                Day2.setText(b.getDay2());
                Day3.setText(b.getDay3());
                Day4.setText(b.getDay4());
                Day5.setText(b.getDay5());
                Day6.setText(b.getDay6());
                Day7.setText(b.getDay7());
                Day8.setText(b.getDay8());
                Day9.setText(b.getDay9());
                Day10.setText(b.getDay10());
                Day1c.setText(b.getcon1());
                Day2c.setText(b.getcon2());
                Day3c.setText(b.getcon3());
                Day4c.setText(b.getcon4());
                Day5c.setText(b.getcon5());
                Day6c.setText(b.getcon6());
                Day7c.setText(b.getcon7());
                Day8c.setText(b.getcon8());
                Day9c.setText(b.getcon9());
                Day10c.setText(b.getcon10());
                Day1h.setText(b.geth1());
                Day2h.setText(b.geth2());
                Day3h.setText(b.geth3());
                Day4h.setText(b.geth4());
                Day5h.setText(b.geth5());
                Day6h.setText(b.geth6());
                Day7h.setText(b.geth7());
                Day8h.setText(b.geth8());
                Day9h.setText(b.geth9());
                Day10h.setText(b.geth10());
                Day1l.setText(b.getl1());
                Day2l.setText(b.getl2());
                Day3l.setText(b.getl3());
                Day4l.setText(b.getl4());
                Day5l.setText(b.getl5());
                Day6l.setText(b.getl6());
                Day7l.setText(b.getl7());
                Day8l.setText(b.getl8());
                Day9l.setText(b.getl9());
                Day10l.setText(b.getl10());

                Day1i.setImage("images/icons/" + b.getI1()+".gif");
                Day2i.setImage("images/icons/" + b.getI2()+".gif");
                Day3i.setImage("images/icons/" + b.getI3()+".gif");
                Day4i.setImage("images/icons/" + b.getI4()+".gif");
                Day5i.setImage("images/icons/" + b.getI5()+".gif");
                Day6i.setImage("images/icons/" + b.getI6()+".gif");
                Day7i.setImage("images/icons/" + b.getI7()+".gif");
                Day8i.setImage("images/icons/" + b.getI8()+".gif");
                Day9i.setImage("images/icons/" + b.getI9()+".gif");
                Day10i.setImage("images/icons/" + b.getI10()+".gif");

                //hashResult.setSize(hashResult.getPreferredSize());
                break;

            case "Clear":
                inputField.setText("");
                shortField.setText("");
                hashResult.setText("");
                weatherResult.setText("");
                icon.setImage("images/icons/blank.gif");
                bg.setImage("images/backgrounds/white.png");
                radar.setImage("images/icons/blankradar.gif");

                Day1.setText("");
                Day2.setText("");
                Day3.setText("");
                Day4.setText("");
                Day5.setText("");
                Day6.setText("");
                Day7.setText("");
                Day8.setText("");
                Day9.setText("");
                Day10.setText("");
                Day1c.setText("");
                Day2c.setText("");
                Day3c.setText("");
                Day4c.setText("");
                Day5c.setText("");
                Day6c.setText("");
                Day7c.setText("");
                Day8c.setText("");
                Day9c.setText("");
                Day10c.setText("");
                Day1h.setText("");
                Day2h.setText("");
                Day3h.setText("");
                Day4h.setText("");
                Day5h.setText("");
                Day6h.setText("");
                Day7h.setText("");
                Day8h.setText("");
                Day9h.setText("");
                Day10h.setText("");
                Day1l.setText("");
                Day2l.setText("");
                Day3l.setText("");
                Day4l.setText("");
                Day5l.setText("");
                Day6l.setText("");
                Day7l.setText("");
                Day8l.setText("");
                Day9l.setText("");
                Day10l.setText("");

                Day1i.setImage("images/icons/blank.gif");
                Day2i.setImage("images/icons/blank.gif");
                Day3i.setImage("images/icons/blank.gif");
                Day4i.setImage("images/icons/blank.gif");
                Day5i.setImage("images/icons/blank.gif");
                Day6i.setImage("images/icons/blank.gif");
                Day7i.setImage("images/icons/blank.gif");
                Day8i.setImage("images/icons/blank.gif");
                Day9i.setImage("images/icons/blank.gif");
                Day10i.setImage("images/icons/blank.gif");

                break;

        }
    }
}